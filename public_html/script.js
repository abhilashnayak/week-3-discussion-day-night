const makeDayBtnElement = document.querySelector('#makeDayBtn');
const makeNightBtnElement = document.querySelector('#makeNightBtn');
const body = document.querySelector('body');
const makeDay = () => {
    body.className = "day";
    makeDayBtnElement.className = "active";
    makeNightBtnElement.className = "";
};
const makeNight = () => {
    body.className = "night";
    makeDayBtnElement.className = "";
    makeNightBtnElement.className = "active";
};
makeDayBtnElement.onclick = makeDay;
makeNightBtnElement.onclick = makeNight;

// Extra Decoration
 const plantationAreaElement = document.querySelector('.plantation-area') ;
 const plants = ['decidious_tree.png', 'evergreen_tree.png', 'palm_tree.png', 'rice.png'];
 for (var i = 0; i < 1920; i += 32) {
    const plantIndex = Math.floor(Math.random() * 4);
    const plantDimention = Math.floor(Math.random() * (128 - 24)) + 24;
    const plant = document.createElement('img');
    plant.setAttribute('src', `./images/${plants[plantIndex]}`);
    plant.setAttribute("height", `${plantDimention}px`);
    plant.setAttribute("width", `${plantDimention}px`);
    plant.style.left= `${i}px`;
    plantationAreaElement.appendChild(plant);
 }
 
